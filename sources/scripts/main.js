var PrimalityTestApp = angular.module('PrimalityTestApp',[])

/**
 * Controllers
 */

PrimalityTestApp.controller('DialogController', ['$scope', function($scope) {
  //Initialization
  $scope.input = ''
  $scope.answer = 'Не определено'

  $scope.test = function () {
    var reg = new RegExp('^\\d+$')
    return $scope.answer = $scope.input == ''
      ? 'не определено'
      : reg.test($scope.input) && isPrime($scope.input)
        ? 'простое'
        : 'непростое'
  }

  /**
   * Tests if number is prime
   *
   * @param {String|Number} n
   * @returns {boolean}
   */
  function isPrime(n) {
    if (n <= 3) { return n > 1 }
    if (n % 2 == 0 || n % 3 == 0) { return false }
    for (var  i = 5; i * i <= n; i += 6) {
      if (n % i == 0 || n % (i + 2) == 0) { return false }
    }
    return true
  }

}])

/**
 * Directives
 */

PrimalityTestApp.directive('isNumber', function () {
  return {
    require: 'ngModel',
    link: function (scope) {
      scope.$watch('input', function(newValue, oldValue) {
        var arr = String(newValue).split("")
        return arr.length === 0
          || arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )
          || arr.length === 2 && newValue === '-.'
          || !isNaN(newValue)
            ? scope.test()
            : (function () {
          scope.input = oldValue
          scope.test()
        })()
      })
    }
  }
})

