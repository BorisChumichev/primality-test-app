var gulp = require('gulp')
  , stylus = require('gulp-stylus')
  , livereload = require('gulp-livereload')
  , jade = require('gulp-jade')
  , uglify = require('gulp-uglify')

gulp.task('stylus', function() {
  gulp.src('sources/styles/*.styl')
    .pipe(stylus({
      compress: true
    }))
    .pipe(gulp.dest('build/css'))
    .pipe(livereload({ auto: false }))
})

gulp.task('jade', function() {
  gulp.src('sources/*.jade')
    .pipe(jade())
    .pipe(gulp.dest('build'))
});

gulp.task('uglify', function() {
  gulp.src('sources/scripts/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('build/js'))
});

gulp.task('watch', function() {
  livereload.listen()
  gulp.watch(['sources/**/**', 'build/**/**'], ['stylus', 'jade', 'uglify'])
})